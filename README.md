# SMUI SvelteKit Exemple

Projet éffectué avec [SvelteKit](https://kit.svelte.dev/).
[Svelte Documentation](https://kit.svelte.dev/docs)

# Le projet SvelteKit ressemble à ceci :

```
my-project/
├ src/
│ ├ lib/
│ │ ├ server/
│ │ │ └ [your server-only lib files]
│ │ └ [your lib files]
│ ├ params/
│ │ └ [your param matchers]
│ ├ routes/
│ │ └ [your routes]
│ ├ app.html
│ ├ error.html
│ └ hooks.js
├ static/
│ └ [your static assets]
├ tests/
│ └ [your tests]
├ package.json
├ svelte.config.js
├ tsconfig.json
└ vite.config.js
```
