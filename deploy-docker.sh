#!/bin/bash

# Nom de l'application
APP_NAME="sveltekite-webapp"
DOCKER_IMAGE="registry.gitlab.com/julien-ilari/sveltekit-personal-webapp:main"

# Définissez une variable pour indiquer si le reset doit être effectué
FORCE_RESET=false
# Vérifiez les arguments passés au script
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --force-reset)
            FORCE_RESET=true
            shift # passer au prochain argument
            ;;
        *)
            # Ignorer les autres arguments inconnus
            shift
            ;;
    esac
done

# Si l'option --force-reset est activée, réinitialisez la base de données
if [ "$FORCE_RESET" = true ]; then
    echo "Réinitialisation de la base de données..."
    # Utilisez la commande "yes" pour envoyer automatiquement "y" comme réponse à la question de Prisma
    yes | npx prisma migrate reset --force
else
   echo "Application des migrations Prismas ..."
   npx prisma migrate deploy
fi

# Nettoyer les images et conteneurs Docker inutilisés
echo "Nettoyage des images et conteneurs Docker inutilisés..."
docker system prune -f

# Exécute le conteneur avec la nouvelle image
cd ~/gitlab-deploy

# Arrête et supprime le conteneur actuel s'il existe
if docker ps -a | grep -q $APP_NAME; then
    docker-compose down
    docker rm $APP_NAME
fi

# Pull de la dernière image depuis GitLab
docker pull $DOCKER_IMAGE
docker-compose up -d

echo "La nouvelle version de $APP_NAME a été démarrée."