#!/bin/bash
# Nom de l'application PM2
APP_NAME="sveltekite-webapp"
APP_PATH=`echo ~/gitlab-deploy/sveltekit-personal-webapp`
APP_DB_URL="file:../dev.db"

# Définissez une variable pour indiquer si le reset doit être effectué
FORCE_RESET=false
# Vérifiez les arguments passés au script
while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --force-reset)
            FORCE_RESET=true
            shift # passer au prochain argument
            ;;
        *)
            # Ignorer les autres arguments inconnus
            shift
            ;;
    esac
done


cd "$APP_PATH"
echo "# [STEP] Installation des dependencies"
npm install
echo "Installing dependencies..."

echo "# [STEP] Déploiement des modifications de la base de données avec Prisma Migrate"
# Vérifier si la variable DATABASE_URL est vide ou non définie
if [ -z "$DATABASE_URL" ]; then
    echo "DATABASE_URL est vide ou non défini."
    export DATABASE_URL="$APP_DB_URL"
    npm i -g prisma@latest
    npm i @prisma/client@latest
fi

# Si l'option --force-reset est activée, réinitialisez la base de données
if [ "$FORCE_RESET" = true ]; then
    echo "Réinitialisation de la base de données..."
    # Utilisez la commande "yes" pour envoyer automatiquement "y" comme réponse à la question de Prisma
    yes | npx prisma migrate reset --force
else
   echo "Application des migrations Prismas ..."
   npx prisma migrate deploy
fi

echo "# [STEP] Reboot de la webapp"
# Vérifier si pm2 est déjà dans le PATH
if ! command -v pm2 &> /dev/null; then
    # pm2 n est pas dans le PATH, nous pouvons l ajouter
    export PATH="$PATH:~/.npm-global/bin"
    echo "pm2 a été ajouté au PATH."
else
    echo "pm2 est déjà dans le PATH."
fi

# Vérifier si l'application PM2 est déjà en cours exécution
if pm2 list|/usr/bin/grep "$APP_NAME"; then
    # Arrêter l'ancienne version
    pm2 delete "$APP_NAME"
    echo "L ancienne version de $APP_NAME a été arrêtée."
else
    echo "Aucune ancienne version de lancée, démarrage de $APP_NAME ..."
fi

# Démarrer la nouvelle version
~/.npm-global/bin/pm2 start server.js --name "$APP_NAME"
echo "La nouvelle version de $APP_NAME a été démarrée."
pm2 ls
