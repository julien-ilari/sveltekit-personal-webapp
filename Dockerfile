# Utilisation d'une image node comme base
FROM node:lts

# Définir le répertoire de travail
WORKDIR /app

# Copier le package.json et package-lock.json pour installer les dépendances
COPY package*.json ./


# Nettoyer le cache npm
RUN npm cache clean --force
# Mettre à jour npm
RUN npm install -g npm
# Installer les dépendances
RUN npm install

# Copier le reste des fichiers
COPY . .

# Construire l'application et suppresion des sources
RUN npm run build
RUN rm -Rf src

# Créez un volume pour le fichier SQLite
VOLUME /app/prisma

# Exposer le port que votre application utilise (si nécessaire) EXPOSE 3000
CMD ["node", "server.js"]