// ts-node script.ts
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function createPage(title, content) {
	try {
		const result = await prisma.wiki.upsert({
			where: { title }, // Spécifiez la clé de recherche
			create: { title, content }, // Valeurs à créer si introuvable
			update: { content } // Valeurs à mettre à jour si trouvée
		});

		console.log('Entrée insérée ou mise à jour :', result);
	} catch (error) {
		console.error("Erreur lors de l'upsert :", error);
	} finally {
		await prisma.$disconnect();
	}
}

async function main() {
	// await prisma.wiki.deleteMany({});
	createPage('Page de test', 'Contenu de test');
	createPage('Page de test 01', 'Contenu de test 01');
	createPage('Page de test 02', 'Contenu de test 02');
	createPage('Page de test 03', 'Contenu de test 03');
}

main()
	.then(async () => {
		await prisma.$disconnect();
	})

	.catch(async (e) => {
		console.error(e);

		await prisma.$disconnect();

		process.exit(1);
	});
