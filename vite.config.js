import { sveltekit } from '@sveltejs/kit/vite';

export default {
	plugins: [sveltekit()],
	server: {
		host: '0.0.0.0', // Écoute sur toutes les interfaces
		port: 4000,
		hot: true
	}
};
